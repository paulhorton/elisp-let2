;;; let2-test.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210605
;; Updated: 20210605
;; Version: 0.0
;; Keywords: 

;;; Commentary:

;;; Change Log:

;;; Code:


(ert-deftest let2-test--list ()
    (should
     (= 5
        (let2 x y '(2 3)
              (+ x y)))))

(ert-deftest let2-test--vector ()
    (should
     (= 9
        (let2 x y [4 5]
          (+ x y)))))


;;; let2-test.el ends here
