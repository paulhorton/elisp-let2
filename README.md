(let2 x y (foo) BODY)
is like (cl-multiple-value-bind (x y) (foo) BODY)
when (foo) returns a list,

But will work when (foo) returns any sequence type.

It is even more similar to (seq-let (x y) (foo) BODY)
Currently differing only in that let2 cl-assert's that
the sequence has at least two elements.

Compare:
```
    (cl-multiple-value-bind (x y) (foo) BODY)
    (seq-let (x y) (foo) BODY)
    (let2 x y (foo) BODY)
```

where (foo) returns a sequence of at least two elements.


when-let2, analogous to when-let is also provided.
