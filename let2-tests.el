;;; let2-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240920
;; Updated: 20240920
;; Version: 0.0
;; Keywords: let2

;;; Commentary:

;;; Change Log:

;;; Code:

(require 'let2)


(ert-deftest let2/test01 ()
  (should
   (=
    5
    (let2 a b (list 2 3)
      (+ a b))
    )))

(ert-deftest when-let2/test01 ()
  (should
   (=
    5
    (let2 a b [2 3 'ignored]
      (+ a b))
    )
   ))


(ert-deftest when-let2/test01 ()
  (should
   (=
    5
    (when-let2 a b [2 3 7]
      (+ a b))
    )))

(ert-deftest when-let2/test02 ()
  (should
   (not
    (when-let2 a b nil
     (+ a b))
   )))



;;; let2-tests.el ends here
