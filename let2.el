;;; let1.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210114
;; Updated: 20210424
;; Version: 0.0
;; Keywords: multiple-value-bind

;;; Commentary:

;; (let2 x y SEQ) is similar to (cl-multiple-value-bind (x y) SEQ)
;; except SEQ can be any sequence type.
;;
;; (let2 x y SEQ) is even more similar to (seq-let (x y) SEQ)
;; except that let2 cl-assert's that SEQ has at least two elements.
;;
;; Generally speaking, seq-let is probably a better choice than let2
;; (I did not know about seq-let when I made let2)
;;
;;
;; Compare:
;;   (cl-multiple-value-bind (x y) (foo) BODY)
;;   (seq-let (x y) (foo) BODY)
;;   (let2 x y (foo) BODY)
;;
;; where (foo) returns a sequence of at least two elements.
;;
;; `when-let2', analogous to `when-let' is also provided.

;; To Consider:
;;   Also accept dotted pairs (x . y)?  Maybe a good idea as seq-let does not accept dotted pairs.
;;   Would need to think about what to do with (x . nil) ≡ (x)

;;; Change Log:

;;; Code:


(defmacro let2 (name1 name2 seq &rest body)
  "Evaluate BODY;
with NAME1 and NAME2 bound to the first two elements of sequence SEQ,
respectively.

If present, extra elements of SEQ are quietly ignored.

Almost the same as `seq-let' and also similar to `cl-multiple-value-bind'

Unlike those, let2 signals error when SEQ has fewer than 2 elemnts.

Example:
  (let2 a b [2 3]
     (+ a b))  <-- Returns 5
"
  (declare (indent 3)
           (debug (symbolp symbolp body)))
  (cl-assert (symbolp name1) t (format "let2 first argument should be a symbol, but got: '%S'" name1))
  (cl-assert (symbolp name2) t (format "let2 first argument should be a symbol, but got: '%S'" name2))
  (cl-assert (consp (car-safe body)) t "let2 macro with nil or atom body makes no sense")
  (let  ((evaled-seq  (make-symbol "--let2--")))
    `(let ((,evaled-seq ,seq))
       (cl-assert ,evaled-seq             nil (format "Error (let2 %S %S...); 3rd arg nil." ',name1 ',name2))
       (cl-assert (seqp ,evaled-seq)      nil (format "Error (let2 %S %S...); 3rd arg '%S', not sequence" ',name1 ',name2 ,evaled-seq))
       (cl-assert (length> ,evaled-seq 1) nil (format "Error (let2 %S %S...); 3rd arg '%S' too short" ',name1 ',name2 ,evaled-seq))
       (let ((,name1  (elt ,evaled-seq 0)))
         (let  ((,name2  (elt ,evaled-seq 1)))
           ,@body
           )))))



(defmacro when-let2 (name1 name2 seq? &rest body)
  "Like `let2' except that if SEQ? is nil just return nil"
  (declare (indent 3)
           (debug (symbolp symbolp body)))
  (cl-assert (symbolp name1) t (format "when-let2 first argument should be a symbol, but got: '%S'" name1))
  (cl-assert (symbolp name2) t (format "when-let2 first argument should be a symbol, but got: '%S'" name2))
  (cl-assert (consp (car-safe body)) t "when-let2 macro with nil or atom body makes no sense")
  (let  ((evaled-seq  (make-symbol "--when-let2--")))
    `(when-let ((,evaled-seq ,seq?))
       (cl-assert (seqp ,evaled-seq)      nil (format "Error (when-let2 %S %S...); 3rd arg '%S', not sequence" ',name1 ',name2 ,evaled-seq))
       (cl-assert (length> ,evaled-seq 1) nil (format "Error (when-let2 %S %S...); 3rd arg '%S' too short" ',name1 ',name2 ,evaled-seq))
       (let ((,name1  (elt ,evaled-seq 0)))
         (let  ((,name2  (elt ,evaled-seq 1)))
           ,@body
           )))))



(provide 'let2)

;;; let2.el ends here
